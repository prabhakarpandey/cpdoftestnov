package com.agiletestingalliance;
import static org.junit.Assert.*;
import org.junit.Test;

public class AboutCPDOFTest {

    @Test
    public void testdesc() throws Exception {

        String k= new AboutCPDOF().desc();
	assertTrue("About ", k.contains("CP-DOF certification"));
    }
}
