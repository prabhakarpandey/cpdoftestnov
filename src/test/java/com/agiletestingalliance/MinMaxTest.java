package com.agiletestingalliance;
import static org.junit.Assert.*;
import org.junit.Test;

public class MinMaxTest  {

    @Test
    public void testadd() throws Exception {

        int k= new MinMax().add(5,3);
	assertEquals("Add", 5, k);
    }

    @Test
    public void testaddone() throws Exception {

        int k= new MinMax().add(3,5);
	assertEquals("Add", 5, k);
    }
}
